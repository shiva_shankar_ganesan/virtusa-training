package com.journaldev.spring.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.journaldev.spring.model.User;
import com.journaldev.spring.service.UserService;

@Controller
public class RegisterController {
	
	@Autowired
	public UserService userService;
	
	private static final long serialVersionUID = 1L;

	@RequestMapping(value="/Register", method= RequestMethod.POST)
	protected ModelAndView Register(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String country = request.getParameter("country");
		User user = new User(email, name, password, country);
		userService.register(user);
		
		String welcomeMsg = "Successfully registered";
		return new ModelAndView("welcome","welcomeMsg", welcomeMsg);
	}

}
