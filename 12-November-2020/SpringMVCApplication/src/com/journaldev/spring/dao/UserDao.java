package com.journaldev.spring.dao;

import com.journaldev.spring.model.Login;
import com.journaldev.spring.model.User;


public interface UserDao {
	
	int register(User user);
	
	User validateUser(Login login);

}
