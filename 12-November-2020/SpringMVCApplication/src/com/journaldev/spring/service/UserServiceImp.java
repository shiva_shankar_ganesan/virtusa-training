package com.journaldev.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.journaldev.spring.dao.UserDao;
import com.journaldev.spring.model.Login;
import com.journaldev.spring.model.User;

@Service
public class UserServiceImp implements UserService {

	@Autowired
	public UserDao userDao;
	
	public int register(User user) {
		return userDao.register(user);
	}
	
	@Override
	public User validateUser(Login login) {
		return userDao.validateUser(login);
		
	}
}
