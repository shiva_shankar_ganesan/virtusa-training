package com.journaldev.spring.service;

import org.springframework.stereotype.Service;

import com.journaldev.spring.model.Login;
import com.journaldev.spring.model.User;

public interface UserService {
	
	int register(User user);
	User validateUser(Login login);

}
