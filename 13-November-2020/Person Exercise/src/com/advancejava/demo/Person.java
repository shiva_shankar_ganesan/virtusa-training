package com.advancejava.demo;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private String firstName;
    private String lastName;
    private int age;

    Person(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this. age = age;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Person> buildPersonObj(){
        List<Person> personObj = new ArrayList<>();
        int i = 0;
        String name= "FirstName";
        String lastName = "lastName";
        while(i <10000){
            String firstName = name+i;
            String lastN = lastName + i;
            int age = i;
            personObj.add(new Person(firstName, lastName, age));
            i++;
        }
        return personObj;
    }

    public boolean isOldAge(Person personOjb){
        if(personOjb.getAge() > 500){
            return true;
        }else{
            return false;
        }

    }

}
