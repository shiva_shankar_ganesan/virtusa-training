package com.advancejava.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PersonDemo {

    public static boolean isOldAge(Person personOjb) throws InterruptedException {
        Thread.sleep(2);
        if(personOjb.getAge() > 50){
            return true;
        }else{
            return false;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        List<Person> personObj = new ArrayList<>();
        int i = 0;
        String name= "FirstName";
        String lastName = "lastName";
        Random rand = new Random();
        while(i <10000){
            String firstName = name+i;
            String lastN = lastName + i;
            int age = rand.nextInt(100);
            personObj.add(new Person(firstName, lastN, age));
            i++;
        }

        List oldPerson = new ArrayList();
        long startTime = System.currentTimeMillis();
        for(Person perObj : personObj){
            if(PersonDemo.isOldAge(perObj)){
                oldPerson.add(perObj);
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime);

        //Stream
        //personObj.parallelStream().filter( a->a.getAge() > 50  ).collect(Collectors.toList());
    }
}