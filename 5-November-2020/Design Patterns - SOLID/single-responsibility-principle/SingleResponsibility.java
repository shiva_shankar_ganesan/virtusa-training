package com.example;

/**
 * Every class should have responsibility over a single part of the functinionality
 * providede by the software and that responsibility should be entirely encapsulated by the class
 */
class Customer{
    String name;
    int age;
    long bill;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getBill() {
        return bill;
    }

    public void setBill(long bill) {
        this.bill = bill;
    }

    Customer(String name , int age){
        this.name = name;
        this.age = age;
    }
}

class BillCalculator{

    public long calculateBill(Customer customer, long tax){
        //Calculate Bill
        return 100;
    }
}

class ReportGenerator{

    public void generateReport(Customer customer, String reportType){
        //Report Generation code
        System.out.println("Report generated");
    }
}


