package com.example;

/**
 * Example for dependency inversion. Loose coupling.
 */
interface Notifier{
    void notify (String message);
}

class EmailNotifier implements Notifier{

    @Override
    public void notify(String message) {
        //Send notification code
        System.out.println("Email Notification sent");
    }
}

class SMSNotifier implements  Notifier{

    @Override
    public void notify(String message) {
        System.out.println("SMS notification sent");
    }
}

class UserManager{

    Notifier notifierObj ;

    public UserManager(Notifier notifier){
        this.notifierObj = notifier;
    }

    public void changePassword(String userName, String oldPassword, String newPassword){
        notifierObj.notify("Password changed");
    }
}

public class DependencyInversion {

    public static void main(String[] args){
        Notifier notifier = new SMSNotifier();
        UserManager userManager= new UserManager(notifier);
        userManager.changePassword("user1", "oldpass", "newpasswprd");
    }
}
