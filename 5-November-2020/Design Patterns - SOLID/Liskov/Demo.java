package com.example.liskov;

/** Derived types must be substitutable for their base types
 * S is a subtype of T, then objects of type T may be replaced with objects of type S
 * Derived types must be completely substitutable for their base types
 */
interface EmployeeBonus{
    int calculateBonus(int salary);
}

interface EmployeeInterface{
    int getMinimumSalary();
}

abstract class Employee implements  EmployeeBonus, EmployeeInterface {

    private int ID;
    private String name;

    public Employee(int id, String name){
        this.ID = id;
        this.name = name;
    }

    public abstract int calculateBonus(int salary);

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Employee{" +  "ID=" + ID + ", name='" + name + '\'' + '}';
    }

}

class PermanentEmployee extends Employee{

    PermanentEmployee(int id, String name) {
        super(id, name);
    }

    public int calculateBonus(int salary) {
        return salary * 10;
    }

    @Override
    public int getMinimumSalary() {
        return 50000;
    }
}

class ContractEmployee implements  EmployeeInterface{

    private int ID;
    private String name;

    ContractEmployee(int id, String name) {
        this.ID = id;
        this.name = name;
    }

    @Override
    public int getMinimumSalary() {
        return 10000;
    }
}

public class Demo {

    public static void main(String[] args){
        EmployeeInterface contractEmployee = new ContractEmployee(1,"Shankar");
        Employee empObj = new PermanentEmployee(2, "Shiva");
        contractEmployee.getMinimumSalary();
        empObj.calculateBonus( empObj.getMinimumSalary());
    }

}
