package com.example;

/**
 *
 One fat interface needs to be split to many smaller and relevant interfaces
 so that clients can know about the interfaces that are relevant to them
 */

interface OrderProcessor{
    boolean validateShippingAddress(String address);
    void processOrder(Object order);
}

interface OnlineOrderProcessorInterface{
    boolean validateCardInfo(String cardInfo);
}

class OnlineOrderProcessor implements  OrderProcessor, OnlineOrderProcessorInterface{


    @Override
    public boolean validateShippingAddress(String address) {
        //Add logic to validate shipping address
        return true;
    }

    @Override
    public void processOrder(Object order) {

        //Process order code

    }

    @Override
    public boolean validateCardInfo(String cardInfo) {
        //validate card info code
        return false;
    }
}

/** CashOnDeliveryOrder implements only OrderProcessor
 */
class CashOnDeliveryOrder implements  OrderProcessor{

    @Override
    public boolean validateShippingAddress(String address) {
        //Validate Shipping Address
        return false;
    }

    @Override
    public void processOrder(Object order) {
        //Process order code
    }
}

public class InterfaceSegregationDemo{

    public static void main(String[] args){

        //if payement method is card -- > Create OnlineOrderProcessor obj
        //If payment method is cash --> Create CashOnDeliveryOrderProcessor obj
    }

}
