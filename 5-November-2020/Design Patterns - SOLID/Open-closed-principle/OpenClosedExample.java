package com.example;

/**
 * Example for Open Closed Principle:
 * The design and writing of the code should be done in a way that new functionality should be
 * added with minimum changes in the existing code
 * New classes for new functionalities, keeping as much as possible existing code unchanged
 */
abstract class Employee{

    private int ID;
    private String name;

    public Employee(int id, String name){
        this.ID = id;
        this.name = name;
    }

    public abstract int calculateBonus(int salary);

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Employee{" +  "ID=" + ID + ", name='" + name + '\'' + '}';
    }

}

//For each employee type we can create a seperate class
class PermanentEmployee extends Employee {

    PermanentEmployee(int id, String name) {
        super(id, name);
    }

    public int calculateBonus(int salary) {
        return salary * 10;
    }
}

//Seperate class can be created for contract employee
//Seperate class can be created for part time employee

public class OpenClosedExample {

        public static void main(String[] args){
            Employee empObj = new PermanentEmployee(1, "Shiva");
            int bonus = empObj.calculateBonus(1000);
            System.out.println(bonus);
        }
}