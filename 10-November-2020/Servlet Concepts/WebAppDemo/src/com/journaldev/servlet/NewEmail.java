package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.journaldev.util.User;

@WebServlet(name = "NewEmail", urlPatterns = { "/NewEmail" })
public class NewEmail extends HttpServlet {
	
static Logger logger = Logger.getLogger(RegisterServlet.class);
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User userObj = (User) session.getAttribute("User");
		String oldEmail = userObj.getEmail();
		String newEmail = request.getParameter("new-email");
		String errorMsg = null;
		if(newEmail == null || newEmail.equals("")){
			errorMsg = "Email ID can't be null or empty.";
		}	
		if(errorMsg != null){
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/ChangeEmail.jsp");
			PrintWriter out= response.getWriter();
			out.println("<font color=red>"+errorMsg+"</font>");
			rd.include(request, response);
		}else {
			//Write the logic here to update the email address

	}
	}

}
