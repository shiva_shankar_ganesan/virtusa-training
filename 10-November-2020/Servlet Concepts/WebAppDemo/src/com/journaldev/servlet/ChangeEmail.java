package com.journaldev.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.journaldev.util.User;

@WebServlet(name = "ChangeEmail", urlPatterns = { "/ChangeEmail" })
public class ChangeEmail extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User userObj = (User) session.getAttribute("User");
		String email = userObj.getEmail();
		response.sendRedirect("ChangeEmail.jsp");
	}
}
