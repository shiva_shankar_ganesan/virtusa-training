package com.gamedesign;

import com.gamedesign.Entity.NPC;

public abstract class NPCAbstractFactory {

     abstract NPC getNPC (String type, String subType);
}