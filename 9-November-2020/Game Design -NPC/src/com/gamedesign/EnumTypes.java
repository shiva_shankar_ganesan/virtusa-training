package com.gamedesign;

public class EnumTypes {

    enum NPCCharacter{
        HUMAN,
        ANIMAL
    }

    enum HumanType{
        VILLAGER,
        SOLDIER
    }

    enum SubType{
        MEN,
        WOMEN,
        SWORDSMEN,
        MARKSMEN
    }

    enum AnimalType{
        HORSE,
        BIRDS
    }

}
