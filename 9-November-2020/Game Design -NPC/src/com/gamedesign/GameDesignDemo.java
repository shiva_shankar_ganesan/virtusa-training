package com.gamedesign;

import com.gamedesign.Entity.Movable;
import com.gamedesign.Entity.NPC;

public class GameDesignDemo {

    public static void main(String[] args){

        //HUMAN NPCS
        NPCAbstractFactory npcFactory = NPCFactoryProducer.getFactory(EnumTypes.NPCCharacter.HUMAN);
        NPC npcObj = npcFactory.getNPC(EnumTypes.HumanType.SOLDIER.toString(), EnumTypes.SubType.MEN.toString());
        npcObj.mobility();
        //npcObj.panicMode();

        //ANIMAL NPCS
        NPCAbstractFactory animalNpcFactory = NPCFactoryProducer.getFactory(EnumTypes.NPCCharacter.ANIMAL);
        NPC animalNpcObj = animalNpcFactory.getNPC(EnumTypes.AnimalType.BIRDS.toString(), null);
        //animalNpcObj.mobility();
       // animalNpcObj.panicMode();

        Movable movObj = NPCPanicFactory.getPanicObj("HUMAN", "SOLDIER");
        movObj.panicMode();

    }
}
