package com.gamedesign.Entity;

public class Horses implements Animals {

    private int age;

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int getAge() {
        return age;
    }


    @Override
    public void mobility() {
        System.out.println("Horses run");
    }

    @Override
    public void panicMode() {
        System.out.println("Horses run faster in panic mode");
    }
}
