package com.gamedesign.Entity;

public class Birds implements Animals {

    private int age;
    @Override
    public void setAge(int age) {
        this.age = age;

    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void mobility() {
        System.out.println("Birds fly");
    }

    @Override
    public void panicMode() {
        System.out.println("Birds fly faster in panic mode");
    }
}
