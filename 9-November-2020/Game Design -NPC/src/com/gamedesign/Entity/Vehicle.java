package com.gamedesign.Entity;

public interface Vehicle {

    public void setVelocity(int velocity);
    public int getVelocity();
}
