package com.gamedesign.Entity;

public class Soldier implements NPC{

    private int age;

    @Override
    public void setAge(int age) {
        this.age = age;

    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void mobility() {
        System.out.println("Soldiers walk");
    }

    @Override
    public void panicMode() {
        System.out.println("Soldiers run faster");
    }


    public enum SoldierType{
        SWORDSMEN,
        MARKSMEN,
        SPIES
    }
}
