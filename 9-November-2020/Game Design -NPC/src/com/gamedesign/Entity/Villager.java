package com.gamedesign.Entity;

public class Villager implements NPC {

    private int age;

    @Override
    public void setAge(int age) {
        this.age = age;

    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void mobility() {
        System.out.println("Villagers walk");

    }

    @Override
    public void panicMode() {
        System.out.println("Villagers run");
    }

    /**
     * Represents the Villager Gender
     */
    public enum Gender{
        MALE,
        FEMALE
    }
}
