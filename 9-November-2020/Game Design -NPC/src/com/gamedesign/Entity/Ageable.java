package com.gamedesign.Entity;

public interface Ageable extends  Movable{

    public void setAge(int age);
    public int getAge();
}
