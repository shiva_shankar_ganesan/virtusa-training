package com.gamedesign;

import com.gamedesign.Entity.NPC;
import com.gamedesign.Entity.Soldier;
import com.gamedesign.Entity.Villager;

public class NPCHumanFactory extends NPCAbstractFactory {

    NPC getNPC(String humanType, String subType) {

        if(humanType.equals(EnumTypes.HumanType.VILLAGER.toString())){
            return new Villager();
        }else if(humanType.equals(EnumTypes.HumanType.SOLDIER.toString())){
            return new Soldier();
        }else{
            return null;
        }
    }

}
