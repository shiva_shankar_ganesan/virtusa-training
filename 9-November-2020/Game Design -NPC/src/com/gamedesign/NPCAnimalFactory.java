package com.gamedesign;

import com.gamedesign.Entity.Birds;
import com.gamedesign.Entity.Horses;
import com.gamedesign.Entity.NPC;
import com.gamedesign.Entity.*;

public class NPCAnimalFactory extends NPCAbstractFactory {

    NPC getNPC(String animal, String subType) {


        if(animal.equals(EnumTypes.AnimalType.HORSE.toString())){
            return new Horses();
        }else if(animal.equals(EnumTypes.AnimalType.BIRDS.toString())){
            return new Birds();
        }else{
            return null;
        }
    }
}
