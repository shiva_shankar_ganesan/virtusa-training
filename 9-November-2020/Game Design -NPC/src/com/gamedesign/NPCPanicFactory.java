package com.gamedesign;

import com.gamedesign.Entity.*;

public class NPCPanicFactory {

    public static Movable getPanicObj(String type, String subType){

        if(type.equals(EnumTypes.NPCCharacter.HUMAN.toString())){
            if(subType.equals(EnumTypes.HumanType.SOLDIER.toString())){
               return new Soldier();
            }else if(subType.equals(EnumTypes.HumanType.VILLAGER.toString())){
                return new Villager();
            }
        }else if(type.equals(EnumTypes.NPCCharacter.ANIMAL.toString())){
            if(subType.equals(EnumTypes.AnimalType.BIRDS.toString())){
                return new Birds();
            }else if(subType.equals(EnumTypes.AnimalType.HORSE.toString())){
                return new Horses();
            }
        }
        return  null;
    }
}
