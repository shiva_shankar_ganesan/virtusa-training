package com.gamedesign;

public class NPCFactoryProducer {

    public static NPCAbstractFactory getFactory(EnumTypes.NPCCharacter type){
        if(type.equals(EnumTypes.NPCCharacter.HUMAN)) {
            return new NPCHumanFactory();
        }else{
            return new NPCAnimalFactory();
        }
    }
}
