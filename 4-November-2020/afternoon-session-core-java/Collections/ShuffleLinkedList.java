package com.example;

import java.util.Collections;
import java.util.LinkedList;

public class ShuffleLinkedList {

    public static void main(String[] args){

        LinkedList listObj = new LinkedList();
        listObj.add("One");
        listObj.add("two");
        listObj.add("three");
        listObj.add("four");
        listObj.add("five");
        System.out.println("Before shuffling" + listObj);
        Collections.shuffle(listObj);
        System.out.println("After Shuffling" + listObj);


    }
}
