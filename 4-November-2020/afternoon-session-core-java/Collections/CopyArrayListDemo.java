package com.example;

import java.util.ArrayList;
import java.util.List;

/**
 * Java Program to copy array List
 */
public class CopyArrayListDemo {

    public ArrayList cloneArrayList(ArrayList arr){

        ArrayList newList =(ArrayList) arr.clone();
        return newList;
    }

    public static void main(String[] args){

        ArrayList<String> fruitList = new ArrayList();
        fruitList.add("Apple");
        fruitList.add("Orange");
        fruitList.add("Grapes");

        CopyArrayListDemo copyList = new CopyArrayListDemo();
        ArrayList newList = copyList.cloneArrayList(fruitList);
        System.out.println(newList);

        List newList2 = new ArrayList(fruitList);
        System.out.println(newList2);
    }
}
