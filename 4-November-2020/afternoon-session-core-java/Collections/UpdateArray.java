package com.example;

import java.util.ArrayList;

/**
 * Java program to update array and arraylist
 */
public class UpdateArray {

    //Update Array
    public void updateArray(int[] arr, int value, int index){

        if(index > arr.length - 1){
            //throw exception
            System.out.println("Index out of bound");
        }else{
            arr[index] = value;
        }

    }

    //Update Array List
    public void updateArrayList(ArrayList list, int index, int value){
        if(index > list.size() -1 ){
            //throw exception
            System.out.println("Index out of bound");
        }else{
            list.set(index, value);
        }
    }

    public static void main(String[] args){

        int[] arr = new int[]{1,2,3,4,5};
        int index = 100;
        int value = 7;
        UpdateArray arrayObj = new UpdateArray();
        arrayObj.updateArray(arr, value, index);

        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        arrayObj.updateArrayList(list, 0, 6);
        System.out.println(list);

    }
}
