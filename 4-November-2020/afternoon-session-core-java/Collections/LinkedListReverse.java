package com.example;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class LinkedListReverse {

    public static void main(String[] args){
        LinkedList carList = new LinkedList<>();
        carList.add("Audi");
        carList.add("BMW");
        carList.add("Toyota");

        //ListIterator
        ListIterator carIterator = carList.listIterator(carList.size());
        while(carIterator.hasPrevious()){
            System.out.println(carIterator.previous());
        }

        //DescendingIterator
        Iterator descendingOrder = carList.descendingIterator();
        while(descendingOrder.hasNext()){
            System.out.println(descendingOrder.next());
        }
    }
}
