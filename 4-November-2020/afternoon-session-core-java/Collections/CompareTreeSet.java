package com.example;

import java.util.TreeSet;

/**
 * Java program to compare two tree set
 */
public class CompareTreeSet {

    public static void main(String[] args){

        TreeSet<String> set1 = new TreeSet<>();
        set1.add("Apple");
        set1.add("Orange");

        TreeSet<String> set2 = new TreeSet<>();
        set2.add("Apple");
        set2.add("Orange");

        if(set1.containsAll(set2)){
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }
    }
}
