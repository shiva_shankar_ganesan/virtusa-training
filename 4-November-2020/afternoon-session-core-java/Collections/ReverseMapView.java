package com.example;

import java.util.TreeMap;

public class ReverseMapView {

    public static void main(String[] args){
        TreeMap <Integer,String> treeMap = new TreeMap<>();
        treeMap.put(1, "One");
        treeMap.put(2, "two");
        treeMap.put(3, "three");

        System.out.println("Originial treemap" + treeMap);
        System.out.println("Reverse order view of the keys" + treeMap.descendingKeySet());
    }
}
