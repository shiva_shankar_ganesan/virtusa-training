package com.example;

import java.util.ArrayList;

public class SearchArrayList {

    public boolean checkForElement(ArrayList arr, int element){
        return arr.contains(element);
    }

    public int indexOfElement(ArrayList arr, int element){
        return arr.indexOf(element);
    }

    public static void main(String[] args){
        ArrayList <Integer> numberList = new ArrayList<>();
        numberList.add(1);
        numberList.add(2);
        numberList.add(3);

        SearchArrayList searchList = new SearchArrayList();
        System.out.println("Element present or not ? " + searchList.checkForElement(numberList, 2));
        System.out.println("Index of the element is " + searchList.indexOfElement(numberList, 2));



    }
}
