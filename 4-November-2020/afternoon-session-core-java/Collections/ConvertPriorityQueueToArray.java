package com.example;

import java.util.PriorityQueue;

/**
 * Convert Priority Queue
 */
public class ConvertPriorityQueueToArray {

    public static void main(String[] args){

        PriorityQueue<String> queue = new PriorityQueue();
        queue.add("One");
        queue.add("Two");
        queue.add("Three");

        System.out.println("Priorty Queue" + queue);

        Object[] stringArray = queue.toArray();

        for(int i=0; i < stringArray.length ; i++){
            System.out.println(stringArray[i]);
        }

    }
}
