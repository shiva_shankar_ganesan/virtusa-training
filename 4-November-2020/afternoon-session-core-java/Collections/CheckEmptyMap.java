package com.example;

import java.util.HashMap;
import java.util.HashSet;

public class CheckEmptyMap {

    public static void main (String[] args){

        HashMap <Integer, String> hashMapObj = new HashMap<>();
        hashMapObj.put(1, "One");
        hashMapObj.put(2,"two");
        hashMapObj.put(3, "three");
        hashMapObj.put(4, "four");

        if(hashMapObj.isEmpty()){
            System.out.println("HashMap is Empty");
        }else{
            System.out.println("Hashmap is not Empty");
        }

    }
}
