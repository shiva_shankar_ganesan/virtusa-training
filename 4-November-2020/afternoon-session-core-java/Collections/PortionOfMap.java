package com.example;

import java.util.TreeMap;

/**
 * Java program to get a portion of a map whose keys are greater than to a given key
 */
public class PortionOfMap {

    public static void main(String[] args){

        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(1, "One");
        treeMap.put(2, "two");
        treeMap.put(3, "three");
        treeMap.put(4, "four");

        System.out.println("Original Tree Map" + treeMap);
        System.out.println("Key greater than 2 " + treeMap.tailMap(2, false));

    }
}
