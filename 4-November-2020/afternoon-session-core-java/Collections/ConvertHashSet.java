package com.example;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Java program to convert hash set to tree set
 */
public class ConvertHashSet {

    public static void main(String[] args){
        Set setObj = new HashSet<>();
        setObj.add(1);
        setObj.add(2);
        setObj.add(3);
        setObj.add(4);
        System.out.println("Hash Set" + setObj);
        Set treeSet = new TreeSet(setObj);
        System.out.println("Tree Set Object"+treeSet);
    }
}
