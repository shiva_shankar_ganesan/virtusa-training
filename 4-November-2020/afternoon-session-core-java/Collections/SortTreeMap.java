package com.example;

import sun.reflect.generics.tree.Tree;

import java.util.Comparator;
import java.util.TreeMap;

/**
 * Java Program to sort Tree Map
 */
public class SortTreeMap {

    public static void main(String[] args){
        TreeMap<String, String> treeMapObj = new TreeMap<>(new CustomSorting());
        treeMapObj.put("A", "one");
        treeMapObj.put("Z", "two");
        treeMapObj.put("F", "three");
        treeMapObj.put("B", "four");
        System.out.println(treeMapObj);
    }
}

class CustomSorting implements Comparator<String>{
    @Override
    public int compare(String s1, String s2){
        return s1.compareTo(s2);
    }
}