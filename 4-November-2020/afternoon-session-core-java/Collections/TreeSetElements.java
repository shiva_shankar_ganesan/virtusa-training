package com.example;

import java.util.TreeSet;

/**
 * Java Program to retrieve element less than the given element
 */
public class TreeSetElements {

    public static void main (String[] args){
        TreeSet<Integer> setObj = new TreeSet<>();
        setObj.add(5);
        setObj.add(6);
        setObj.add(7);

        System.out.println(setObj.lower(6));
    }
}
