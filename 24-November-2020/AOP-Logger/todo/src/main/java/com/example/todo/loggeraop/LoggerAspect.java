package com.example.todo.loggeraop;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggerAspect {

    Logger log = LoggerFactory.getLogger(LoggerAspect.class);

    @Pointcut(value = "execution(* com.example.todo.*.*.*(..))")
    public void myPointCut(){

    }

    @Around("myPointCut()")
    public Object applicationLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        ObjectMapper mapper = new ObjectMapper();
        String methodName = joinPoint.getSignature().getName();
        String className  = joinPoint.getClass().toString();
        Object[] arguments = joinPoint.getArgs();
        log.info("method invoked" + className + " : " + methodName + "()" + "arguments: "+ mapper.writeValueAsString(arguments));
        Object object = joinPoint.proceed();
        log.info("Response"  + mapper.writeValueAsString(object));
        return object;
    }

}
