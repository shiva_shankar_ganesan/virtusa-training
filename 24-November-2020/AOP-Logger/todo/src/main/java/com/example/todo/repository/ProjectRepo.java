package com.example.todo.repository;
import com.example.todo.model.entity.ProjectEntity;
import com.example.todo.model.entity.TodoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProjectRepo extends JpaRepository<ProjectEntity, Integer> {


}
