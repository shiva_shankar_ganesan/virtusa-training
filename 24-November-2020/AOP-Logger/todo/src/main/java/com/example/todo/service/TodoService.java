package com.example.todo.service;

import com.example.todo.model.Project;
import com.example.todo.model.Todo;

import java.util.List;

public interface TodoService {

    List<Todo> getTodosForProject(int projectId);
    public int createTodo(Todo todo, int projectId);
    String deleteTodoById(int projectId, int todoId);
    String updateTodoById(int projectId, int todoId, Todo todoObj);
}
