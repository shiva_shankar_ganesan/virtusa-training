package com.example.todo.util;

import com.example.todo.model.Project;
import com.example.todo.model.Todo;
import com.example.todo.model.entity.ProjectEntity;
import com.example.todo.model.entity.TodoEntity;
import com.example.todo.repository.ProjectRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectUtil {
    
    public static Project convertEntityToDTO(ProjectEntity entityObj){
        Project dtoObj = new Project();
        BeanUtils.copyProperties(entityObj, dtoObj);
        List<TodoEntity> todos = entityObj.getTasks();
        List<Todo> newTodo = new ArrayList<>();
        for(TodoEntity todoEntity: todos){
            Todo todoObj = new Todo();
            todoObj.setTodoText(todoEntity.getTodoText());
            todoObj.setTodoId(todoEntity.getTodoId());
            todoObj.setCompletedStatus(todoEntity.isCompletedStatus());
            newTodo.add(todoObj);
        }
        dtoObj.setTasks(newTodo);
        return dtoObj;
    }

    public static Todo convertEntityToDTO(TodoEntity entityObj){
        Todo dtoObj = new Todo();
        BeanUtils.copyProperties(entityObj, dtoObj);
        return dtoObj;
    }

    public static ProjectEntity convertDTOToEntity(Project dtoObj){
        ProjectEntity entityObj = new ProjectEntity();
        BeanUtils.copyProperties(dtoObj, entityObj);
        return entityObj;
    }

    public static TodoEntity convertDTOToEntity(Todo dtoObj){
        TodoEntity entityObj = new TodoEntity();
        BeanUtils.copyProperties(dtoObj, entityObj);
        return entityObj;
    }




}
