package com.example.todo.service;

import com.example.todo.exception.ApiRequestException;
import com.example.todo.model.Project;
import com.example.todo.model.Todo;
import com.example.todo.model.entity.ProjectEntity;
import com.example.todo.model.entity.TodoEntity;
import com.example.todo.repository.ProjectRepo;
import com.example.todo.repository.TodoRepo;
import com.example.todo.util.ProjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectRepo projectRepo;

    @Autowired
    TodoRepo todoRepo;


    @Override
    public List<Project> getAllProjects() {
        return projectRepo.findAll().stream().map(ProjectUtil::convertEntityToDTO).collect(Collectors.toList());
    }

    @Override
    public int createProject(Project project) {
        ProjectEntity projectEntity= ProjectUtil.convertDTOToEntity(project);
        ProjectEntity persistedObj = projectRepo.save(projectEntity);
        return persistedObj.getProjectId();
    }


    @Override
    public Project getProjectById(int projectID) {
        ProjectEntity projectEntity =  projectRepo.findById(projectID).orElseThrow(()-> new ApiRequestException("Project ID not found"));
        Project project= ProjectUtil.convertEntityToDTO(projectEntity);
        return  project;
    }

    @Override
    public String deleteProjectById(int projectId) {
        projectRepo.findById(projectId).orElseThrow(()-> new ApiRequestException("Project ID not found"));
        projectRepo.deleteById(projectId);
        return "Deleted Successfully";
    }

    @Override
    public String updateProjectById(Project project, int projectId) {
        ProjectEntity projectEntity = projectRepo.findById(projectId).orElseThrow(()-> new ApiRequestException("Project ID not found"));
        projectEntity.setTitle(project.getTitle());
        List<TodoEntity> todoEntityList = new ArrayList<>();
        List<Todo> todos = project.getTasks();
        for(Todo todo : todos){
            TodoEntity todoEntity = todoRepo.findById(todo.getTodoId()).orElseThrow( () -> new ApiRequestException("Incorrect todo ID sent in the request"));
            todoEntity.setTodoText(todo.getTodoText());
            todoEntity.setCompletedStatus(todo.isCompletedStatus());
            todoEntityList.add(todoEntity);
        }
        projectEntity.setTasks(todoEntityList);
        projectRepo.save(projectEntity);
        return "Updated Successfully";
    }
}
