package com.example.todo.controller;


import com.example.todo.model.Project;
import com.example.todo.model.Todo;
import com.example.todo.model.entity.ProjectEntity;
import com.example.todo.repository.ProjectRepo;
import com.example.todo.service.ProjectService;
import com.example.todo.service.TodoService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/projects")
public class ProjectController {

    @Autowired
    TodoService todoService;

    @Autowired
    ProjectService projectService;

    @GetMapping
    public List<Project> getAllProjects(){
        return projectService.getAllProjects();
    }

    @PostMapping
    public ResponseEntity<Void> createProject(@RequestBody Project project){
        int projectId =  projectService.createProject(project);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("x-project-id", Integer.toString(projectId))
                .build();
    }

    @GetMapping(value = "/{id}")
    public Project getProjectById(@PathVariable("id") int projectId){
        return  projectService.getProjectById(projectId);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteProjectById(@PathVariable("id") int projectId){
         projectService.deleteProjectById(projectId);
         return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> updateProject(@RequestBody Project project, @PathVariable("id")int projectId){
        projectService.updateProjectById(project, projectId);
        return ResponseEntity.noContent().build();
    }

}
