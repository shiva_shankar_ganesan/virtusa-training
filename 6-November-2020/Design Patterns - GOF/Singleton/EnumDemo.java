package com.demo.SingletonDemo;

public class EnumDemo {

    public static void main(String[] args){
        SingletonEnum singleton1 = SingletonEnum.INSTANCE;
        singleton1.setValue(2);

        SingletonEnum singleton2 = SingletonEnum.INSTANCE;
        singleton2.setValue(5);
        System.out.println("Object 1 value after changing the value in object2: " + singleton1.getValue());
        System.out.println("Object 2 value: " + singleton2.getValue());
    }
}
