package com.advancedjava.assignment;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Question 4
 */
public class StringLength {

    public static Map<Integer, List<String>> normalGroupStringLength(List<String> stringList){
        Map<Integer, List<String>> map = new HashMap<>();
        for(String str : stringList){
            int length = str.length();
            if(map.containsKey(length)){
                List<String> list = map.get(length);
                list.add(str);
                map.put(length, list);
            }else{
                List<String> list= new ArrayList<String>();
                list.add(str);
                map.put(length,list);
            }
        }
        return map;
    }

    //Java 8
    public static Map<Integer, List<String>> groupStringLength(List<String> stringList){

        Map<Integer, List<String>> groupMap= new HashMap<>(
                stringList.stream().collect(Collectors.groupingBy(String::length))
        );
        return groupMap;
    }

    public static void main(String[] args){

        List<String> stringList = new ArrayList<>();
        stringList.add("One");
        stringList.add("two");
        stringList.add("three");
        stringList.add("eight");
        System.out.println(StringLength.groupStringLength(stringList));
    }
}
