package com.advancedjava.assignment;

import java.util.ArrayList;
import java.util.List;

/**
 * Question 1: Give a List<Integer> as the input, find average of all elements
 */
public class ListAverage {

    public static double calculateAverage(List<Integer> numbersList) {
        Integer sum = 0;
        if (numbersList != null || !numbersList.isEmpty()) {
            for (Integer number : numbersList) {
                sum += number;
            }
            return sum.doubleValue() / numbersList.size();
        }
        return sum;
    }

    public static double findAverageUsingStreams(List<Integer> numbersList){
        return numbersList.stream().mapToInt(i -> i).average().orElse(0);
    }

    public static void main(String[] args){
        List<Integer> numbersList = new ArrayList<>();
        numbersList.add(1);
        numbersList.add(2);
        numbersList.add(4);
        System.out.println(ListAverage.calculateAverage(numbersList));
        System.out.println(ListAverage.findAverageUsingStreams(numbersList));
    }

}
