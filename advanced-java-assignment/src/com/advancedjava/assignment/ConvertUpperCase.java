package com.advancedjava.assignment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Question 6: Folder with multiple files. read and convert them to uppercase
 */

public class ConvertUpperCase {

    public static void main(String[] args) throws IOException {

        /*File folder = new File("C:\\Users\\shganesan\\IdeaProjects\\advanced-java-assignment\\files");
        File[] listOfFiles = folder.listFiles();
        try {
            FileWriter writerObj = new FileWriter("Output.txt");
            for (File file : listOfFiles) {
                Scanner reader = new Scanner(file);
                while (reader.hasNextLine()) {
                    String data = reader.nextLine();
                    writerObj.write(data.toUpperCase());
                    writerObj.write("\n");
                }
                reader.close();
            }
            writerObj.close();
        }catch (IOException e){
            e.printStackTrace();
        }*/

        List<String> outputText = new ArrayList<>();
        Path outputPath = Paths.get("finalOutput.txt");
        Stream<Path> paths = Files.walk(Paths.get("C:\\Users\\shganesan\\IdeaProjects\\advanced-java-assignment\\files"));
        paths.filter(Files::isRegularFile).forEach(fileobj -> {
            try {
                Files.lines(fileobj).forEach(line -> outputText.add(line.toUpperCase()));
                Files.write(outputPath, outputText);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }
}
