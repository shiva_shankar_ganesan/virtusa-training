package com.advancedjava.assignment;

import java.util.HashMap;
import java.util.Map;

/**
 * Question 3. Search the ggiven map for key
 */
public class SearchMap {

    public static String normalPrintMapKeyValue(Map<String, String> map, String searchKey){
        if(map.containsKey(searchKey)){
            return map.get(searchKey);
        }else{
            return "NOT_FOUND";
        }
    }

    //Java 8
    public static String printMapKeyValue(Map<String, String> map, String searchKey){
        return map.getOrDefault(searchKey, "NOT_FOUND");
    }

    public static void main(String[] args){
        Map<String, String> map = new HashMap<>();
        map.put("1" , "One");
        map.put("2", "two");
        map.put("3", "three");
        System.out.println(SearchMap.normalPrintMapKeyValue(map, "1"));
        System.out.println(SearchMap.printMapKeyValue(map, "five"));
    }
}
