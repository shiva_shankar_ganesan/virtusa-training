package com.advancedjava.assignment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Question 1.2: Return a single list of string all the strings from the list of lists given as the input
 */
public class FlattenList {

    public static List<String> getFlattenList ( List<List<String>> listOfListOfString){
        List<String> listOfString = new ArrayList<>();
        for(List<String> value: listOfListOfString){
            for(String str : value){
                listOfString.add(str);
            }
        }
        return listOfString;
    }

    public static List<String> getFlattenListUsingForEach(List<List<String>> listOfListOfString){
        List<String> ls = new ArrayList<>();
        listOfListOfString.forEach(ls::addAll);
        return ls;
    }

    public static void main(String[] args){

        List<List<String>> listOfListOfString = new ArrayList<>();
        listOfListOfString.add(Arrays.asList("A", "B", "C"));
        listOfListOfString.add(Arrays.asList("D", "E", "F"));

        System.out.println("Before flattening" + listOfListOfString);
        System.out.println("Normal method " + FlattenList.getFlattenList(listOfListOfString));
        System.out.println("Using ForEach" + FlattenList.getFlattenListUsingForEach(listOfListOfString));
    }
}
