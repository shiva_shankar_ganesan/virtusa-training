package com.advancedjava.assignment;

import java.util.HashMap;
import java.util.Map;

/**
 * Question 2: Print map key and value
 */
public class PrintMapKeyValue {

    public static void normalPrintMapKey(Map<String, String> map){

        for(Map.Entry<String, String> entry :map.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println("Key: " + key + " - " + value);
        }
    }
    //Java 8
    public static void printMapKeyValue(Map<String, String> map){
        map.forEach( (key, value) -> System.out.println("Key: "+ key + " - " + value));
    }

    public static void main(String[] args){

        Map<String, String> map = new HashMap<>();
        map.put("1", "One");
        map.put("2" , "Two");
        map.put("3", "Three");

        PrintMapKeyValue.normalPrintMapKey(map);
        PrintMapKeyValue.printMapKeyValue(map);
    }
}
