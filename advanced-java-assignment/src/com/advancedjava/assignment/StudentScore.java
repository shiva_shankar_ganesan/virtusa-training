package com.advancedjava.assignment;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Question 5: Best Student Score
 */
class Student{
    int id;
    Map<String , Integer> report;

    public Student(int id, Map<String, Integer> report){
        this.id = id;
        this.report = report;
    }

    @Override
    public String toString() {
        return "Student{" + "id=" + id + "}";
    }
}

enum Subject{
    SCIENCE, MATHEMATICS, LANGUAGE, HISTORY
}

class Score{
    public String subject;
    public Student student;

    public Score(String subject, Student student){
        this.subject = subject;
        this.student = student;
    }

    @Override
    public String toString() {
        return "Score{" + "subject='" + subject  + ", student=" + student.toString() + '}';
    }
}

class sortByScore implements Comparator<Score>{

    @Override
    public int compare(Score o1, Score o2) {
        Integer o1_student_score = o1.student.report.get(o1.subject);
        Integer o2_student_score = o2.student.report.get(o2.subject);
        if( o1_student_score == o2_student_score){
            return new Integer(o1.student.id).compareTo(new Integer(o2.student.id));
        }else{
            return o2_student_score.compareTo(o1_student_score);
        }
    }
}


public class StudentScore {

    public static Map<String, List<Student>> getBestScores(List<Student> students){

        Map<String , List<Score>> studentBySubject = students.stream()
                .flatMap(s->{
                    List<Score> result = new ArrayList<>();
                    for( String subject : s.report.keySet()){
                        result.add(new Score(subject, s));
                    }
                    return result.stream();
                })
                .collect(Collectors.groupingBy(sc -> sc.subject));

        Map<String, List<Student>> result = new HashMap<>();
        for(String subject: studentBySubject.keySet()){
            List<Student> sortedStudents = studentBySubject.get(subject).stream()
                    .sorted(new sortByScore())
                    .map(sc -> sc.student)
                    .collect(Collectors.toList());
            result.put(subject, sortedStudents);
        }
        return result;
    }
    public static void main(String[] args){
        Map<String, Integer> student1Report = new HashMap<>();
        student1Report.put(Subject.SCIENCE.toString(), 60);
        student1Report.put(Subject.MATHEMATICS.toString(), 90);
        student1Report.put(Subject.LANGUAGE.toString(), 60);
        student1Report.put(Subject.HISTORY.toString() , 60);

        Map<String, Integer> student2Report = new HashMap<>();
        student2Report.put(Subject.SCIENCE.toString(), 70);
        student2Report.put(Subject.MATHEMATICS.toString(), 70);
        student2Report.put(Subject.LANGUAGE.toString(), 70);
        student2Report.put(Subject.HISTORY.toString() , 70);

        Student s1 = new Student(1, student1Report);
        Student s2 = new Student(2, student2Report);
        List<Student> studentList = new ArrayList<>();
        studentList.add(s1);
        studentList.add(s2);
        System.out.println(StudentScore.getBestScores(studentList));
    }

}
