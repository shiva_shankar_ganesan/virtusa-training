package com.example.todo.service;

import com.example.todo.exception.ApiRequestException;
import com.example.todo.model.Project;
import com.example.todo.model.Todo;
import com.example.todo.model.entity.ProjectEntity;
import com.example.todo.model.entity.TodoEntity;
import com.example.todo.repository.ProjectRepo;
import com.example.todo.repository.TodoRepo;
import com.example.todo.util.ProjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoServiceImpl implements TodoService{

    @Autowired
    ProjectRepo projectRepo;

    @Autowired
    TodoRepo todoRepo;



    public List<Todo> getTodosForProject(int projectId){
        ProjectEntity projectEntity =  projectRepo.findById(projectId).orElseThrow( () -> new ApiRequestException("Project ID not found"));
        Project projObj = ProjectUtil.convertEntityToDTO(projectEntity);
        return projObj.getTasks();
    }

    public int createTodo(Todo todo, int projectId){
        TodoEntity todoEntity = ProjectUtil.convertDTOToEntity(todo);
        TodoEntity persistedTodo = todoRepo.save(todoEntity);
        ProjectEntity projectEntity = projectRepo.findById(projectId).orElseThrow(()-> new ApiRequestException("Project ID not found"));
        List<TodoEntity> listTodoEntities = projectEntity.getTasks();
        listTodoEntities.add(todoEntity);
        projectRepo.save(projectEntity);
        return persistedTodo.getTodoId();
    }

    @Override
    public String deleteTodoById(int projectId, int todoId) {
       ProjectEntity projectEntity =  projectRepo.findById(projectId).orElseThrow(()->new ApiRequestException("Project ID not found"));
       List<TodoEntity> todoEntityList =projectEntity.getTasks();
       boolean remove = todoEntityList.removeIf(todoEntity -> todoEntity.getTodoId() == todoId);
       if(!remove){
           throw new ApiRequestException("Todo Id is not found for the given Project ID");
       }
       projectEntity.setTasks(todoEntityList);
       projectRepo.save(projectEntity);
       todoRepo.deleteById(todoId);
       return "Deleted Successfully";
    }

    @Override
    public String updateTodoById(int projectId, int todoId, Todo todo) {
        ProjectEntity projectEntity =  projectRepo.findById(projectId).orElseThrow(()->new ApiRequestException("Project ID not found"));
        TodoEntity oldtodoEntity = todoRepo.findById(todoId).orElseThrow(()-> new ApiRequestException("Todo ID invalid"));

        List<TodoEntity> todoEntityList =projectEntity.getTasks();
        if(!todoEntityList.contains(oldtodoEntity)) throw new ApiRequestException("Invalid todoId for given project ID");

        TodoEntity newTodoEntity = ProjectUtil.convertDTOToEntity(todo);
        for(TodoEntity todoEntity : todoEntityList){
            if(todoEntity.getTodoId() == todoId){
              todoEntityList.remove(todoEntity);
              todoEntityList.add(newTodoEntity);
            }
        }
        projectEntity.setTasks(todoEntityList);
        projectRepo.save(projectEntity);
        todoRepo.save(newTodoEntity);
        return "Updated Successfully";
    }


}
