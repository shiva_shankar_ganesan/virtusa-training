package com.example.todo.controller;

import com.example.todo.model.Todo;
import com.example.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/projects")
public class TodoController {

    @Autowired
    TodoService todoService;

    @PostMapping(value = "/{id}/todos")
    public ResponseEntity<Void> createTodo(@RequestBody Todo todo, @PathVariable("id")int projectId){
        int todoId =  todoService.createTodo(todo, projectId);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("x-todo-id", Integer.toString(todoId))
                .build();
    }

    @GetMapping(value="/{id}/todos")
    public List<Todo> getTodosForProject(@PathVariable("id") int id){
        return todoService.getTodosForProject(id);
    }

    @PutMapping(value = "/{id}/todos/{todoId}")
    public ResponseEntity<Void> updateTodo(@PathVariable("id") int projectId, @PathVariable("todoId") int todoId, @RequestBody Todo todo){
         todoService.updateTodoById(projectId, todoId, todo);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value="/{id}/todos/{todoId}")
    public ResponseEntity<Void> deleteTodos(@PathVariable("id") int projectId, @PathVariable("todoId") int todoId){
        todoService.deleteTodoById(projectId, todoId);
        return ResponseEntity.noContent().build();
    }
}
