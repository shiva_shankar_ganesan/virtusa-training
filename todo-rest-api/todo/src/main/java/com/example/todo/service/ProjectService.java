package com.example.todo.service;

import com.example.todo.model.Project;

import java.util.List;

public interface ProjectService {

    List<Project> getAllProjects();
    int createProject(Project project);
    public Project getProjectById(int projectID);
    public String deleteProjectById(int projectId);
    public String updateProjectById(Project project, int projectId);
}
