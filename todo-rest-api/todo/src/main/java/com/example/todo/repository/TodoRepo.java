package com.example.todo.repository;
import com.example.todo.model.entity.TodoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepo extends JpaRepository<TodoEntity, Integer> {


}
