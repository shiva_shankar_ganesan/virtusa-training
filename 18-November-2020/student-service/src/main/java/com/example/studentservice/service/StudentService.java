package com.example.studentservice.service;

import com.example.studentservice.model.Student;

import java.util.List;

public interface StudentService {

    List<Student> getStudents();

    Student getStudentById(int id);

    List<Student> getStudentByName(String firstName);
}
