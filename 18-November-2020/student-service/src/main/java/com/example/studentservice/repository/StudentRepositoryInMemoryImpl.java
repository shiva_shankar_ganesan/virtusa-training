package com.example.studentservice.repository;

import com.example.studentservice.model.Student;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class StudentRepositoryInMemoryImpl implements StudentRepository {

    final List<Student> students;
    public StudentRepositoryInMemoryImpl(){
        students = new ArrayList<>();
        students.add(new Student(1, "John", "David"));
        students.add(new Student(2, "Anna", "Mathews"));
        students.add(new Student(2, "Arun", "Raj"));
    }

    @Override
    public List<Student> getAllStudents() {
        return students;
    }

}
