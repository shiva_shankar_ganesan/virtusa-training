package com.example.studentservice.service;

import com.example.studentservice.model.Student;
import com.example.studentservice.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getStudents() {
        return studentRepository.getAllStudents();
    }

    @Override
    public Student getStudentById(int id) {
        List<Student> studentList = studentRepository.getAllStudents();
        Optional<Student> matchingObj = studentList.stream().filter(student -> student.getId() == id).findAny();
        Student studentObj = matchingObj.get();
        return studentObj;
    }

    @Override
    public List<Student> getStudentByName(String name) {
        List<Student> studentList = studentRepository.getAllStudents();
        return studentList.stream().filter((student -> student.getFirstName().contains(name) || student.getLastName().contains(name))).collect(Collectors.toList());
    }
}
