package com.example.studentservice.repository;


import com.example.studentservice.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository {

    List<Student> getAllStudents();
}
