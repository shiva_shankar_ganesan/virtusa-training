package com.example.studentservice.controller;

import com.example.studentservice.model.Student;
import com.example.studentservice.repository.StudentRepo;
import com.example.studentservice.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentRepo studentRepo;

    @GetMapping
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

    @GetMapping(value="/id={id}")
    public Student getStudentById(@PathVariable("id") String id){
        return studentService.getStudentById(Integer.parseInt(id));
    }

    @GetMapping(value="/name={firstName}")
    public List<Student> getStudentsByName(@PathVariable("firstName") String firstName){
        return studentService.getStudentByName(firstName);
    }


    @PostMapping("/addStudent")
    public String addStudent(@RequestBody Student student){
        studentRepo.save(student);
        return "Student added to the database";
    }

    @GetMapping("/getAllStudents")
    public List<Student> getAll(){
        return studentRepo.findAll();
    }
    @GetMapping("/getStudentById/{id}")
    public Optional<Student> getAll(@PathVariable ("id") String id){
        return studentRepo.findById(Integer.parseInt(id));
    }




}
