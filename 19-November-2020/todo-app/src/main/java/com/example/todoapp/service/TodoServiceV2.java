package com.example.todoapp.service;

import com.example.todoapp.model.TodoV2;

import java.util.List;

public interface TodoServiceV2 {

    public List<TodoV2> getTodos();
    public String addTodo(TodoV2 todo);
}
