package com.example.todoapp.controller;
import com.example.todoapp.model.Todo;
import com.example.todoapp.model.TodoV2;
import com.example.todoapp.service.TodoServiceImplV2;
import com.example.todoapp.service.TodoServiceV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v2/todo")
public class TodoV2Controller {

    @Autowired
    TodoServiceV2 todoServiceV2;

    @GetMapping
    public List<TodoV2> getTodos(){
        return todoServiceV2.getTodos();
    }

    @PostMapping
    public String addTodo(@RequestBody TodoV2 todo){
        return todoServiceV2.addTodo(todo);
    }

}
