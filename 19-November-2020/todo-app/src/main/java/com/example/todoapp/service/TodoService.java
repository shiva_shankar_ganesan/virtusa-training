package com.example.todoapp.service;

import com.example.todoapp.model.Todo;

import java.util.List;
import java.util.Map;

public interface TodoService {

    List<Todo> getTodos();
    Todo getTodoById(int id);
    String addTodo(Todo todo);
    String updateTodo(Todo todo);
    String patchTodo(int id, Map fields);
    String deleteTodo(int id);
}
