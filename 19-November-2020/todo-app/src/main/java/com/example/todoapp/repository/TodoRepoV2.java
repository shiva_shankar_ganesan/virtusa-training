package com.example.todoapp.repository;

import com.example.todoapp.model.TodoV2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepoV2 extends JpaRepository<TodoV2, Integer> {
}
