package com.example.todoapp.service;

import com.example.todoapp.model.Todo;
import com.example.todoapp.model.TodoV2;
import com.example.todoapp.repository.TodoRepoV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoServiceImplV2 implements  TodoServiceV2{

    @Autowired
    TodoRepoV2 todoRepoV2;

    public List<TodoV2> getTodos() {
        return todoRepoV2.findAll();
    }

    public String addTodo(TodoV2 todo) {
        todoRepoV2.save(todo);
        return "Added successfully";
    }
}
