package com.example.todoapp.exception;

import net.bytebuddy.implementation.bytecode.Throw;

public class ApiRequestException extends RuntimeException{

    public ApiRequestException(String message){
        super(message);
    }

    public ApiRequestException(String message, Throwable cause){
        super(message, cause);
    }

}
