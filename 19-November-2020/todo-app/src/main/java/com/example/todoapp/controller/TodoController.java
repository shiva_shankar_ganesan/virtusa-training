package com.example.todoapp.controller;

import com.example.todoapp.model.Todo;
import com.example.todoapp.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/todo")
public class TodoController {

    @Autowired
    TodoService todoService;

    @GetMapping
    public List<Todo> getTodos(){
        return todoService.getTodos();
    }

    @PostMapping
    public String addTodo(@RequestBody Todo todo){
        return todoService.addTodo(todo);
    }

    @GetMapping(value = "{id}")
    public Todo getTodoById(@PathVariable ("id") String id ){
        return todoService.getTodoById(Integer.parseInt(id));
    }

    @PutMapping(value="{id}")
    public String updateTodo(@RequestBody Todo todo){
        return todoService.updateTodo(todo);
    }

    @PatchMapping(value="{id}")
    public String patchTodo(@PathVariable String id, @RequestBody Map<Object, Object> field){
        return  todoService.patchTodo(Integer.parseInt(id), field);
    }

    @DeleteMapping(value="{id}")
    public String deleteTodo(@PathVariable ("id") String id){
        return todoService.deleteTodo(Integer.parseInt(id));
    }


}
