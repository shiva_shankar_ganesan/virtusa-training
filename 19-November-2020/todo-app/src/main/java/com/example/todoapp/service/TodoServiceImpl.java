package com.example.todoapp.service;

import com.example.todoapp.exception.ApiRequestException;
import com.example.todoapp.model.Todo;
import com.example.todoapp.repository.TodoRepo;
import com.sun.el.util.ReflectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    TodoRepo todoRepository;

    @Override
    public List<Todo> getTodos() {
        return todoRepository.findAll();
    }

    @Override
    public Todo getTodoById(int id) {
        return todoRepository.findById(id).orElseThrow(()-> new ApiRequestException("No todo found by the given ID"));
    }

    @Override
    public String addTodo(Todo todo) {
        todoRepository.save(todo);
        return "Added successfully";
    }

    @Override
    public String updateTodo(Todo todo) {
        Todo oldTodoObj = todoRepository.findById(todo.getId()).orElseThrow(()-> new ApiRequestException("No Todo Found by ID"));
        String text = todo.getText();
        oldTodoObj.setText(text);
        todoRepository.save(oldTodoObj);
        return "Updated successfully";
    }

    @Override
    public String deleteTodo(int id){
        todoRepository.findById(id).orElseThrow(()-> new ApiRequestException("No todo found by ID to delete"));
        todoRepository.deleteById(id);
        return "Deleted successfully";
    }

    @Override
    public String patchTodo(int id, Map fields) {
       Todo todoObj = todoRepository.findById(id).orElseThrow(()-> new ApiRequestException("No todo found for the given ID"));
       fields.forEach((k,v)->{
           Field field = ReflectionUtils.findField(Todo.class, (String)k);
           field.setAccessible(true);
           ReflectionUtils.setField(field, todoObj, v);
       });

       todoRepository.save(todoObj);
       return "Updated successfully";
    }

}
