package com.demo.rxjava;


import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ObservableObserverDemo {

    public static void createObservableUsingCreate(){
        Observable<String> source = Observable.create( emitter -> {
            try{
                emitter.onNext("One");
                emitter.onNext("two");
                emitter.onNext("three");
            }catch (Throwable e){
                emitter.onError(e);
            }
        });
        source.subscribe(s-> System.out.println("Received :"+ s), Throwable::printStackTrace);
        source.map(String::length)
                .filter(i-> i>3)
                .subscribe(s-> System.out.println("Recieved :" + s));

    }

    public static void createObservableUsingJust(){
        Observable<String> source = Observable.just("one", "two", "three");
        source.subscribe(s-> System.out.println("Received:" +s));
    }
    
    public static void createUsingIterable(){
        List<String> items = new ArrayList<>();
        items.add("One");
        items.add("two");
        items.add("three");
        Observable<String> source = Observable.fromIterable(items);
        source.subscribe(s-> System.out.println("Received :" + s));
    }

    public static void coldObservable() throws InterruptedException {
        Observable<Long> source = Observable.interval(1, TimeUnit.SECONDS);
        source.subscribe( item -> System.out.println("Observer 1: "+item));
        Thread.sleep(3000);
        source.subscribe(item -> System.out.println("Observer 2: "+item));
        Thread.sleep(5000);
    }

    public static void hotObservable() throws InterruptedException{

        Observable<Long> source = Observable.interval(1, TimeUnit.SECONDS);
        ConnectableObservable<Long> connectableObservable = source.publish();
        connectableObservable.subscribe( item -> System.out.println("Observer 1: " + item));
        connectableObservable.connect();
        Thread.sleep(3000);
        connectableObservable.subscribe( item -> System.out.println("Observer 2:" + item));
        Thread.sleep(5000);

    }

    public static void main(String[] args) throws InterruptedException {

        //createObservableUsingCreate();
       //createObservableUsingJust();
        // createUsingIterable();
       //coldObservable();
        hotObservable();

    }

}
