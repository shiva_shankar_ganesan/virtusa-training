package com.demo.rxjava;

import io.reactivex.Observable;

import java.util.Objects;

public class BasicOperators {

    public static void conditionalOperators(){

        //onComplete event is generated when the emissions is more than 5
        Observable.range(1, 100)
                .takeWhile(i -> i <5)
                .subscribe(i -> System.out.println("RECIEVED: " + i));

        //SKIP WHILE
        Observable.range(1, 100)
                .skipWhile(i -> i<=95)
                .subscribe(i -> System.out.println("RECIEVED " +i));

        //defaultIfEmpty()
        Observable<String> items = Observable.just("One", "Two");
        items.filter(s-> s.startsWith("Z"))
                .defaultIfEmpty("None")
                .subscribe(System.out::println);

        //SwitchIfEmpty
        Observable.just("One", "two", "three")
                .filter(s -> s.startsWith("f"))
                .switchIfEmpty(Observable.just("four", "five", "six"))
                .subscribe(i-> System.out.println("Recieved: " + i));
    }

    public static void suppressingOperators(){

        //FILTER
        Observable.just("One", "two", "three")
                .filter(s-> s.length() !=3)
                .subscribe(s-> System.out.println("RECEIVED: " + s));

        //TAKE
        Observable.just("One", "two", "three", "four")
                .take(2)
                .subscribe(s -> System.out.println("RECIEVED: " +s));

        //SKIP
        Observable.range(1,100)
                .skip(90)
                .subscribe(s -> System.out.println("RECIEVED: " +s));

        //DISTINCT
        Observable.just("One", "two", "three")
                .map(String::length)
                .distinct()
                .subscribe(i -> System.out.println("RECIEVED: " + i));

        //DISTINCTUNTILCHANGED
        Observable.just(1,1,1,2,2,3,3,2,1,1)
                .distinctUntilChanged()
                .subscribe(i -> System.out.println("RECEIVED: " +i));

        //ELEMENT AT
        Observable.just("One", "two", "three", "four", "five")
                .elementAt(2)
                .subscribe(i -> System.out.println("RECIEVED: " + i));
    }

    public static void transformingOperators(){

        //MAP
        Observable.just("One", "two", "three")
                .map(String::length)
                .subscribe(i -> System.out.println("RECIEVED: " + i));

        //CAST : Casts each emitted item to another type
        Observable<Object> items = Observable.just("One", "two", "three").cast(Object.class);

        //STARTWITHITEM
        Observable<String> menu = Observable.just("Coffee", "tea", "Latte");
        menu.startWith("COFFEE SHOP MENU")
                .subscribe(System.out::println);

        //SORTED
        Observable.just(5,7,3,1,6,0,4)
                .sorted()
                .subscribe(System.out::println);

        //SCAN
        Observable.just(5,3,7)
                .scan((accumulator, i) -> accumulator+i)
                .subscribe(s -> System.out.println("Received:" +s));

    }

    public static void reducingOperators(){

        //Count
        Observable.just("One", "two", "three")
                .count()
                .subscribe(s-> System.out.println("Received: " +s));

        //REDUCE
        Observable.just(5,3,7)
                .reduce((total, i) -> total+i)
                .subscribe(s -> System.out.println("Received: " +s));
    }

    public static void booleanOperator(){
        //ALL
        Observable.just(5,3,7,11,2,14)
                .all( i -> i <10)
                .subscribe(s -> System.out.println("Received: " +s));

        //isEmpty
        Observable.just("One", "Two", "Three")
                .filter(s -> s.contains("z"))
                .isEmpty()
                .subscribe(s -> System.out.println("Recieved: " +s));

        //contains
        Observable.range(1, 100)
                .contains(50)
                .subscribe(s -> System.out.println("Received: " +s));


    }

    public static void collectionOperator(){

    }

    public static void main(String[] args){
        //conditionalOperators();
        //suppressingOperators();
        //transformingOperators();
        //reducingOperators();
        booleanOperator();
    }
}
