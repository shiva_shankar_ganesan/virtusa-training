package com.demo.rxjava;


import io.reactivex.Observable;
import io.reactivex.Observer;

public class ReactiveOperators {

    public static void mergeOperator(){
        Observable<String> src1 = Observable.just("One", "two", "three");
        Observable<String> src2 = Observable.just("Four", "five");

        Observable.merge(src1, src2)
                .subscribe( i -> System.out.println("Recieved: " + i));

        //Operator version
        src1.mergeWith(src2)
                .subscribe(i -> System.out.println("Recieved: "+ i));

        Observable<String> source = Observable.just("Alpha", "Beta" , "Gamma");
        source.flatMap(s -> Observable.fromArray(s.split(""))).subscribe(System.out::println);
    }

    public static void concatOperator(){
        Observable<String> src1 = Observable.just("One", "two", "three");
        Observable<String> src2 = Observable.just("Four", "five");
        Observable.concat(src1, src2)
                .subscribe(i -> System.out.println("Received: " +i));
    }

    public static void zipOperator(){
        Observable<String> src1 = Observable.just("One", "two", "three");
        Observable<Integer> src2 = Observable.range(1, 6);
        Observable.zip(src1, src2, (s,i) -> s + "-" + i)
                .subscribe(System.out::println);
    }

    public static void main(String[] args){

        //mergeOperator();
        //concatOperator();
        zipOperator();
    }
}
